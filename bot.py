import telebot

bot = telebot.TeleBot('2013806645:AAEyOSs7A3Nd-wB0_kz77NZAfI_q-Yej2EE')
sticker_id = 'CAACAgIAAxkBAAMfYUUOfL6lHoLFC0AkKA0a1SXMO2QAAuMPAALjGylKYFt7Dyasf4IgBA'


def create_mention(user):
    if user.username:
        mention = '@{}'.format(user.username)
    else:
        mention = '<a href="tg://user?id={}">{}</a>'.format(user.id, user.first_name)
    return mention


@bot.message_handler(commands=['start', 'help'])
def command_start_help(message):
    bot.send_message(message.chat.id, 'Привіт! Я видалю всі голосові з твоїх групових бесід!')


# @bot.message_handler(content_types=['audio'])
@bot.message_handler(content_types=['voice'])
def handle_voice(message):
    if message.chat.type == 'private':
        return
    tg_user = message.from_user
    mention = create_mention(tg_user)
    bot.delete_message(message.chat.id, message.id)
    bot.send_message(message.chat.id, f'{mention}, голосове видалено!')
    bot.send_sticker(message.chat.id, sticker_id)


@bot.message_handler(content_types=["new_chat_members"])
def handle_added_to_chat(message):
    if message.new_chat_members[0].username == 'no_voice_here_bot':
        bot.reply_to(message, 'Не забудьте надати мені права адміністратора та дозвіл на видалення повідомлень!')


@bot.message_handler(content_types=["sticker"])
def test(message):
    print('a')


if __name__ == '__main__':
    print("Started!")
    bot.polling()
